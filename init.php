<?php
/**
 * LQDI Light Toolkit
 * Copyright © LQDI Technologies - 2011
 * http://www.lqdi.net
 *
 * Inicialização do toolkit
 * Inclui todas as bibliotecas e arquivos de configurações necessários
 *
 * @author Aryel Tupinambá
 */
 
include("lib/Utils.php");
include("lib/Log.php");
include("lib/LDB.php");

include("config.php");
include("functions.php");
?>
