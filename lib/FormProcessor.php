<?php

/**
 * LQDI Light Toolkit 2.0
 * Copyright © LQDI Technologies - 2011
 * http://www.lqdi.net
 *
 * Biblioteca de processamento e validação de formulários
 *
 * @author Aryel 'DfKimera' Tupinambá
 * 
 */

class FormProcessor {

	/**
	 * @var array $data O buffer interno de processamento do formulário
	 */
	private static $form = NULL;
	private static $data = array();

	/**
	 * Define qual formulário deve ser processado
	 * @param array &$input A array de entrada do formulário (tipicamente $_POST)
	 */
	public static function setFormInput(&$input) {
		self::$form = & $input;
	}

	/**
	 * Processa um campo do formulário com um determinado filtro/mutator/validator
	 * @param string $fieldName O nome do campo de formulário
	 * @param string $mutator O nome da função mutator
	 * @param string $propertyName O nome da propriedade no novo formulário mutado
	 */
	public static function processField($fieldName, $mutator, $propertyName = NULL) {
		if (self::$form == NULL) {
			error("Cannot process field: form input was not defined or is null");
			return;
		}

		if ($propertyName == NULL) {
			$propertyName = $fieldName;
		}

		$mutator = "mutator_{$mutator}";

		self::$data[$propertyName] = self::$mutator(self::$form[$fieldName]);
	}
	
	/**
	 * Processa uma seleção de data composta por 3 tags <select>
	 * @param string $fieldName O prefixo do nome do campo de formulário (seguido de _day, _month e _year)
	 * @param string $propertyName O nome da propriedade no novo formulário mutado
	 */
	public static function processDateSelect($fieldNamePrefix, $mutator, $propertyName = NULL) {
		if (self::$form == NULL) {
			error("Cannot process field: form input was not defined or is null");
			return;
		}

		$mutator = "mutator_{$mutator}";
		
		$date_day = str_pad(intval(self::$form["{$fieldNamePrefix}_day"]), 2, "0", STR_PAD_LEFT);;
		$date_month = str_pad(intval(self::$form["{$fieldNamePrefix}_month"]), 2, "0", STR_PAD_LEFT);;
		$date_year = str_pad(intval(self::$form["{$fieldNamePrefix}_year"]), 4, "0", STR_PAD_LEFT);

		self::$data[$propertyName] = self::$mutator("{$date_day}/{$date_month}/{$date_year}");
	}

	/**
	 * Processa uma série de checkbox e extrai um valor decimal representando as flags
	 * @see http://stackoverflow.com/questions/2773914/c-php-storing-settings-in-an-integer-using-bitwise-operators
	 * @param string $prefix O prefixo dos campos no formulário
	 * @param int $begin O número do checkbox inicial
	 * @param int $end O número do checkbox final
	 * @param string $attributeName O nome do atributo à armazenar o valor das flags
	 */
	public static function processCheckbox($prefix, $begin, $end, $attributeName) {
		$prefixLen = strlen($prefix);
		$flags = 0;
		foreach (self::$form as $key => $value) {
			$kp = substr($key, 0, $prefixLen);
			if ($kp == $prefix) {
				$index = intval(substr($key, $prefixLen));
				$flags += pow(2, $index - 1);
			}
		}

		self::$data[$attributeName] = $flags;
	}

	/**
	 * Obtém os dados do formulário, com mutação
	 * @return array Uma array com todos os dados do formulário, após mutação
	 */
	public static function getData() {
		return self::$data;
	}

	/**
	 * Extrai do formulário uma série de campos
	 * @param array $fields Array com os nomes dos campos desejados
	 * @return array Uma array com os dados após filtragem e mutação.
	 * Apenas as propriedades que existirem no form existirão na array
	 */
	public static function getSpecificData($fields = array()) {
		$filteredData = array();
		foreach ($fields as $fieldName) {
			if (isset(self::$data[$fieldName])) {
				$filteredData[$fieldName] = self::$data[$fieldName];
			}
		}

		return $filteredData;
	}

	/**
	 * Obtém o valor de um campo específico do formulário, após mutação
	 * @param string $fieldName O nome do campo
	 * @return object O valor do campo, após mutação
	 */
	public static function getFieldValue($fieldName) {
		return self::$data[$fieldName];
	}

	/**
	 * Atualiza um Model com os valores do formulário
	 * @param Model $model O modelo à ser atualizado
	 * @param array $mapping Um mapeamento de campos de formulário X propriedades do model (KV)
	 */
	public static function updateModel(Model &$model, $mapping = NULL) {
		foreach (self::$data as $key => $value) {
			if (property_exists($model, $key)) {

				if ($mapping != NULL && isset($mapping[$key])) {
					$property = $mapping[$key];
				} else {
					$property = $key;
				}

				$model->$property = $value;
			}
		}
	}

	private static function mutator_sqlString($value) {
		return clear($value);
	}

	private static function mutator_sqlBBCode($value) {
		return clear(strip_tags($value));
	}

	private static function mutator_sqlInt($value) {
		return intval($value);
	}

	private static function mutator_sqlFloat($value) {
		return floatval($value);
	}

	private static function mutator_sqlDatepicker($value) {

		$value = strval($value);

		if (strlen($value) < 10) {

			$value = "01/01/1970";
		}

		$dt = new DateTime(Utils::fixEUDate($value));

		return $dt->format("Y-m-d");
	}

	private static function mutator_sqlGender($value) {
		$value = trim($value);
		if ($value != "M") {
			$value = "F";
		} else {
			$value = "M";
		}
		return $value;
	}

}

?>
