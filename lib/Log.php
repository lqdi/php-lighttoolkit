<?php
/**
 * LQDI Light Toolkit 2.0
 * Copyright © LQDI Technologies - 2011
 * http://www.lqdi.net
 *
 * Biblioteca de logging
 *
 * @author Aryel Tupinambá
 */

class Log {

	public static $logwrites = array();
	public static $logfile = NULL;
	public static $networkMode = false;

	/**
	 * Define se o serviço de logging deve operar sobre o serviço de Network
	 *
	 * @param  bool $networkMode
	 */
	public static function SetNetworkMode($networkMode) {

		self::$networkMode = true;
	}

	/**
	 * Define o caminho do arquivo de escrita do log
	 *
	 * @param  string $file
	 */
	public static function SetFile($file) {
		self::$logfile = $file;
	}

	/**
	 * Escreve uma mensagem no log da aplicação
	 *
	 * @param  string $message
	 */
	public static function Write($message) {

		if (self::$logfile == NULL) {
			self::$logfile = "data/logs/" . date("Y-m-d") . ".log";
		}

		$t = debug_backtrace();
		$caller = basename($t[sizeof($t) - 1]['file']) . ":" . $t[sizeof($t) - 1]['line'];

		$string = "\r\n[L][" . date("Y-m-d @ H:i:s") . "][{$caller}] : {$message}";
		@error_log($string, 3, self::$logfile);

		if (self::$networkMode) {
			Network::Log($string);
		}

		array_push(self::$logwrites, $message);

		return $message;
	}

	/**
	 * Escreve no log todos os dados de um objeto
	 *
	 * @param  string $message
	 */
	public static function Object($object) {

		if (self::$logfile == NULL) {
			self::$logfile = "data/logs/" . date("Y-m-d") . ".log";
		}

		$t = debug_backtrace();
		$caller = basename($t[sizeof($t) - 1]['file']) . ":" . $t[sizeof($t) - 1]['line'];

		$message = "Dumping object: " . $object . "\r\n";
		ob_start();
		var_dump($object);
		$message .= "\r\n" . ob_get_contents();
		ob_end_clean();
		$message .= "\r\n";

		$string = "\r\n[O][" . date("Y-m-d @ H:i:s") . "][{$caller}] : {$message}";
		@error_log($string, 3, self::$logfile);

		if (self::$networkMode) {
			Network::Log($string);
		}

		array_push(self::$logwrites, $message);
		return $message;
	}

	/**
	 * Executa um backtrace da execução e armazena no log
	 *
	 * @param  bool $show_string_args Define se os parâmetros do tipo string em cada função deve ser armazenada no log
	 */
	public static function Trace($show_string_args = false) {
		$buffer = "";

		if (self::$logfile == NULL) {
			self::$logfile = "data/logs/" . date("Y-m-d") . ".log";
		}

		$t = debug_backtrace();

		$buffer .= $string = "\r\n[T][" . date("Y-m-d @ H:i:s") . "] Current backtrace:";
		@error_log($string, 3, self::$logfile);

		// We do multiple error_log calls because of the max. log message size of 1024.
		// A long execution stack and a lot of arguments in a function could easily
		// pass this value. We don't need to care about performance here because
		// this is only for debugging purposes.

		foreach ($t as $n => $i) {
			$buffer .= $string = "\r\n -> [{$n}] {$i['file']}:{$i['line']}";

			$argList = array();
			foreach ($i['args'] as $argNum => $arg) {
				if (is_string($arg) && strlen($arg) > 36) {
					if ($show_string_args) {
						array_push($argList, "'{$arg}'");
					} else {
						array_push($argList, "'[string]" . strlen($arg) . "'");
					}
				} else if (is_resource($arg)) {
					array_push($argList, "'[resource] " . print_r($arg, true) . "'");
				} else if (is_array($arg)) {
					array_push($argList, "'[array] " . sizeof($arg) . "'");
				} else {
					array_push($argList, "'{$arg}'");
				}
			}

			$argList = join(", ", $argList);

			$buffer .= $string .= "\r\n \t {$i['function']}( {$argList} );";
			@error_log($string, 3, self::$logfile);
		}

		$buffer .= $string = "\r\n[T] End of backtrace ----------------";
		@error_log($string, 3, self::$logfile);

		array_push(self::$logwrites, $buffer);
		
		return $buffer;

	}

}

?>