<?php
/**
 * LQDI Light Toolkit
 * Copyright © LQDI Technologies - 2011
 * http://www.lqdi.net
 *
 * Global function library
 * Biblioteca de funções globais
 *
 * @author Aryel Tupinambá
 */

/**
 * Redireciona o navegador do usuário para uma URL
 * @param string $url A URL a redirecionar
 */
function redirect($url) {
	echo "<meta http-equiv=\"refresh\" content=\"0;url={$url}\" />";
	exit();
}

/**
 * Codifica um ID de URL para passagem segura via URL
 * @param int O ID original
 * @return string O ID codificado
 */
function encodeID($id) {
	return substr(base64_encode(intval($id) *1320),0);
}

/**
 * Decodifica um ID codificado
 * @param string $enc ID codificado
 * @return int O ID decodificado
 */
function decodeID($enc) {
	return intval(intval(base64_decode($enc)) /1320);
}

/**
 * Envia uma resposta AJAX (JSON) para o navegador do usuario.
 * 
 * @param string $status O status da requisição
 * @param array $parameters Parametros ou dados a serem retornados
 */
function reply($status, $parameters = array()) {
	discard_output();
	
	$data = $parameters;
	$data['status'] = $status;
	
	ob_start();
	die( json_encode( $data ));
	ob_end_flush();
	exit();
	
}

/**
 * Corrige linebreaks literais para tags "<br />"
 * @param string $str A string a ser corrigida
 */
function fixLinebreaks($str) {
	return str_replace("\\n","<br />",$str);
}

/**
 * Remove todas as linebreaks literais da string
 * @param string $str A string a ser corrigida
 */
function stripLinebreaks($str) {
	return str_replace("\\n"," ",$str);
}
/**
 * Inicializa o banco de dados com os dados da variável de config global "$db"
 */
function start_database() {
	global $db;
	
	$connID = LDB::Connect($db['hostname'],$db['username'],$db['password'],$db['database']);
	return $connID;
}

/**
 * Atalho para a função mysql_escape_string()
 * @param string $str A string original
 * @return string A string escapada
 */
function clear($str) {
	return mysql_escape_string($str);
}

/**
 * Imprime na tela uma mensagem com quebra de linha e conversão UTF-8
 * @param string $msg A mensagem a ser impressa
 */
function trace($msg) {
	echo utf8_decode("<br>{$msg}");
}

/**
 * Descarta o buffer de saída atual
 */
function discard_output() {
	while(ob_get_length () !== FALSE) {
		ob_end_clean();
	}
}

?>
